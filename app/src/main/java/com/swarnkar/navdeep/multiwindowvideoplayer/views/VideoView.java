package com.swarnkar.navdeep.multiwindowvideoplayer.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by navdeep on 9/1/16.
 */
public class VideoView extends SurfaceView implements SurfaceHolder.Callback {

    private MediaPlayer mediaPlayer;
    private SurfaceHolder holder;
    private String path;

    public VideoView(Context context) {
        super(context);
        mediaPlayer = new MediaPlayer();
    }

    public VideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mediaPlayer = new MediaPlayer();
    }

    public VideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mediaPlayer = new MediaPlayer();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public VideoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mediaPlayer = new MediaPlayer();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setVideoPath(String path) {
        this.path = path;
    }

    public void start() {
        if (!TextUtils.isEmpty(path)) {
            try {
                mediaPlayer.setDataSource(path);
                this.holder = getHolder();
                this.holder.addCallback(this);
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                        //mp.setDisplay(VideoView.this.holder);
                    }
                });
                mediaPlayer.prepareAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void pause() {
        if (mediaPlayer.isPlaying())
            mediaPlayer.pause();
    }

    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    public void resume() {
        mediaPlayer.start();
    }

    public void stop() {
        mediaPlayer.stop();
    }

    public void seekTo(int msec) {
        mediaPlayer.seekTo(msec);
    }

    public int getVideoHeight() {
        return mediaPlayer.getVideoHeight();
    }

    public int getVideoWidth() {
        return mediaPlayer.getVideoWidth();
    }

    public int getCurrentPosition() {
        return mediaPlayer.getCurrentPosition();
    }

    public int getDuration() {
        return mediaPlayer.getDuration();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mediaPlayer.setDisplay(this.holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener listener) {
        mediaPlayer.setOnCompletionListener(listener);
    }
}