package com.swarnkar.navdeep.multiwindowvideoplayer.services;

import android.app.Service;
import android.content.Intent;
import android.content.res.Resources;
import android.os.IBinder;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by navdeep on 6/1/16.
 */
public class WindowService extends Service {

    protected WindowManager windowManager;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
    }

    public void addView(View v, WindowManager.LayoutParams params) {
        windowManager.addView(v, params);
    }

    public void removeView(View v) {
        windowManager.removeViewImmediate(v);
    }

    public void updateViewLayout(View v, WindowManager.LayoutParams params) {
        windowManager.updateViewLayout(v, params);
    }

    public int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    protected double getDistance(float x1, float y1, float x2, float y2) {
        return Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
    }

    @Override
    public void onDestroy() {
    }
}