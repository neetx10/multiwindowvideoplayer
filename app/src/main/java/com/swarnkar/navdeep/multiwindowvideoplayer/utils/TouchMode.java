package com.swarnkar.navdeep.multiwindowvideoplayer.utils;

/**
 * Created by navdeep on 12/5/16.
 */
public enum TouchMode {
    HEADER_DRAG, RESIZE_VIEW, VIDEO_VIEW_DRAG_ZOOM
}