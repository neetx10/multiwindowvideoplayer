package com.swarnkar.navdeep.multiwindowvideoplayer.services;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.crash.FirebaseCrash;
import com.swarnkar.navdeep.multiwindowvideoplayer.R;
import com.swarnkar.navdeep.multiwindowvideoplayer.utils.CommonUtils;
import com.swarnkar.navdeep.multiwindowvideoplayer.utils.TouchMode;
import com.swarnkar.navdeep.multiwindowvideoplayer.views.VideoView;

import java.io.File;

/**
 * Created by navdeep on 6/1/16.
 */
public class PlayerService extends WindowService {

    private int w, h;
    private TouchMode touchMode = TouchMode.HEADER_DRAG;

    @Override
    public void onCreate() {
        super.onCreate();
        w = windowManager.getDefaultDisplay().getWidth();
        h = windowManager.getDefaultDisplay().getHeight();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String path = "";
        try {
            path = intent.getStringExtra("path");
        } catch (Exception e) {
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
        if (!TextUtils.isEmpty(path)) {
            startPlayer(path);
        }
        return START_STICKY;
    }

    void startPlayer(final String path) {
        final Boolean[] visible = {true};
        LayoutInflater inflater = LayoutInflater.from(this);
        final View view = inflater.inflate(R.layout.video_layout, null);
        final VideoView videoView = (VideoView) view.findViewById(R.id.video);
        final TextView title = (TextView) view.findViewById(R.id.title);
        File file = new File(path);
        if(!TextUtils.isEmpty(file.getName()))
            title.setText(file.getName());
        videoView.setFocusable(false);
        videoView.setFocusableInTouchMode(false);
        videoView.setVideoPath(path);
        videoView.start();
        Button close = (Button) view.findViewById(R.id.close);
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED,
                PixelFormat.TRANSLUCENT
        );
        w = windowManager.getDefaultDisplay().getWidth();
        h = windowManager.getDefaultDisplay().getHeight();
        params.width = w / 2;
        params.height = (int) ((float) params.width * 9 / 16) + getResources().getDimensionPixelSize(R.dimen.header_height);
        params.gravity = Gravity.CENTER;

        final View header = view.findViewById(R.id.header);
        header.setFocusableInTouchMode(true);
        header.setFocusable(true);
        final View controllerView = view.findViewById(R.id.controller_view);
        controllerView.setVisibility(View.GONE);
        final ImageButton playButton = (ImageButton) view.findViewById(R.id.play);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videoView.isPlaying()) {
                    videoView.pause();
                    playButton.setImageResource(android.R.drawable.ic_media_play);
                } else {
                    videoView.resume();
                    playButton.setImageResource(android.R.drawable.ic_media_pause);
                    controllerView.setVisibility(View.GONE);
                }
            }
        });
        final Handler controllerHandler = new Handler();
        final Runnable controllerRunnable = new Runnable() {
            @Override
            public void run() {
                if (videoView.isPlaying()) {
                    controllerView.setVisibility(View.GONE);
                }
            }
        };
        header.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                touchMode = TouchMode.HEADER_DRAG;
                return false;
            }
        });
        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (touchMode != TouchMode.RESIZE_VIEW) {
                    touchMode = TouchMode.VIDEO_VIEW_DRAG_ZOOM;
                    if (controllerView.getVisibility() != View.VISIBLE) {
                        controllerView.setVisibility(View.VISIBLE);
                        controllerHandler.postDelayed(controllerRunnable, 2000);
                    }
                }
                return false;
            }
        });
        final View resizeView = view.findViewById(R.id.resizeView);
        resizeView.setFocusableInTouchMode(true);
        resizeView.setFocusable(true);
        resizeView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                touchMode = TouchMode.RESIZE_VIEW;
                return false;
            }
        });
        addView(view, params);
        final int resizeHeight = getResources().getDimensionPixelSize(R.dimen.resize_dimen);
        final int headerHeight = getResources().getDimensionPixelSize(R.dimen.header_height);
        final int resizeWidth = getResources().getDimensionPixelSize(R.dimen.resize_dimen);
        view.setOnTouchListener(new View.OnTouchListener() {

            int x = 0, y = 0, d = 0, touchX = 0, touchY = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (touchMode == TouchMode.HEADER_DRAG) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            x = params.x == 0 ? view.getWidth() / 2 : (int) (event.getRawX() - params.x);
                            x = x < 0 ? view.getWidth() / 2 : x;
                            break;
                        case MotionEvent.ACTION_MOVE:
                            w = windowManager.getDefaultDisplay().getWidth();
                            params.x = (int) event.getRawX() - (x) <= 0 ? 0 : (int) event.getRawX() - (x);
                            params.x = params.x > (w - view.getWidth()) ? (w - view.getWidth()) : params.x;
                            params.y = (int) event.getRawY() - header.getHeight() - header.getHeight() / 2;
                            params.gravity = Gravity.LEFT | Gravity.TOP;
                            windowManager.updateViewLayout(view, params);
                            break;
                        default:
                            touchMode = TouchMode.HEADER_DRAG;
                            break;
                    }
                } else if (touchMode == TouchMode.RESIZE_VIEW) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            x = params.x;
                            y = params.y;
                            break;

                        case MotionEvent.ACTION_MOVE:
                            w = windowManager.getDefaultDisplay().getWidth();
                            h = windowManager.getDefaultDisplay().getHeight();
                            if (params.gravity == Gravity.CENTER) {
                                params.x = w / 2 - view.getWidth() / 2;
                                params.y = h / 2 - view.getHeight() / 2;
                                x = params.x;
                                y = params.y;
                                params.gravity = Gravity.LEFT | Gravity.TOP;
                            } else {
                                int lh = view.getHeight();
                                int lw = view.getWidth();
                                params.gravity = Gravity.LEFT | Gravity.TOP;
                                params.width = (lw + (int) event.getRawX() - x - lw);
                                params.height = lh + (int) event.getRawY() - y - lh - resizeHeight;
                                params.width = CommonUtils.clamp(params.width, resizeWidth, (w - params.x));
                                params.height = CommonUtils.clamp(params.height, resizeHeight + headerHeight, (h - params.y));
                                updateViewLayout(view, params);
                                videoView.getHolder().setFixedSize(videoView.getWidth(), videoView.getHeight());
                            }
                            break;
                        default:
                            touchMode = TouchMode.HEADER_DRAG;
                            break;
                    }
                } else if (touchMode == TouchMode.VIDEO_VIEW_DRAG_ZOOM) {
                    if (event.getPointerCount() == 2) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                d = (int) getDistance(event.getX(0), event.getY(0), event.getX(1), event.getY(1));
                                break;
                            case MotionEvent.ACTION_MOVE:
                                if (params.gravity == Gravity.CENTER) {
                                    params.x = w / 2 - view.getWidth() / 2;
                                    params.y = h / 2 - view.getHeight() / 2;
                                    x = params.x;
                                    y = params.y;
                                    params.gravity = Gravity.LEFT | Gravity.TOP;
                                    updateViewLayout(view, params);
                                } else {
                                    int d1 = (int) getDistance(event.getX(0), event.getY(0), event.getX(1), event.getY(1));
                                    if (d == 0) {
                                        d = d1;
                                    } else {
                                        w = windowManager.getDefaultDisplay().getWidth();
                                        h = windowManager.getDefaultDisplay().getHeight();
                                        params.gravity = Gravity.LEFT | Gravity.TOP;
                                        params.height += d1 - d;
                                        params.width += d1 - d;
                                        params.width = CommonUtils.clamp(params.width, resizeWidth, (w - params.x));
                                        params.height = CommonUtils.clamp(params.height, resizeHeight, (h - params.y));
                                        updateViewLayout(view, params);
                                        d = d1;
                                    }
                                    videoView.getHolder().setFixedSize(videoView.getWidth(), videoView.getHeight());
                                }
                                break;
                            default:
                                d = 0;
                                break;
                        }
                    } else {
                        d = 0;
                        w = windowManager.getDefaultDisplay().getWidth();
                        h = windowManager.getDefaultDisplay().getHeight();
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                x = params.x;
                                y = params.y;
                                touchX = (int) event.getRawX();
                                touchY = (int) event.getRawY();
                                break;
                            case MotionEvent.ACTION_MOVE:
                                if (params.gravity == Gravity.CENTER) {
                                    x = params.x;
                                    y = params.y;
                                    touchX = (int) event.getX();
                                    touchY = (int) event.getY();
                                    params.gravity = Gravity.LEFT | Gravity.TOP;
                                } else {
                                    params.x += (event.getRawX() - params.x) - (touchX - x);
                                    params.y += (event.getRawY() - params.y) - (touchY - y);
                                    params.x = CommonUtils.clamp(params.x, 0, w - (view.getWidth() - (touchX - x)));
                                    params.y = CommonUtils.clamp(params.y, 0, h - (view.getHeight() - (touchY - y)));
                                    params.gravity = Gravity.LEFT | Gravity.TOP;
                                    updateViewLayout(view, params);
                                }
                                break;
                        }
                    }
                }
                return true;
            }
        });
        final SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekBar);
        seekBar.setMax(videoView.getDuration());
        final Handler seekHandler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (videoView.isPlaying()) {
                    seekBar.setMax(videoView.getDuration());
                    seekBar.setProgress(videoView.getCurrentPosition());
                }
                if (visible[0])
                    seekHandler.postDelayed(this, 1000);
            }
        };
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            boolean first = true;

            @Override
            public void onCompletion(MediaPlayer mp) {
                if (first) {
                    first = false;
                    return;
                }
                seekBar.setProgress(videoView.getDuration());
                playButton.setImageResource(android.R.drawable.ic_media_play);
                controllerView.setVisibility(View.VISIBLE);
            }
        });
        seekHandler.postAtTime(runnable, 1000);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    videoView.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visible[0] = false;
                videoView.stop();
                removeView(view);
                System.gc();
            }
        });
    }
}